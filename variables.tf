variable "aws_region" {
  type = string
}

variable "my_access_key" {
  description = "User's Access Key ID"
  type        = string
  sensitive   = true
}

variable "my_secret_key" {
  description = "User's Secret access key"
  type        = string
  sensitive   = true
}

variable "nonprod_vpc_cidr" {}
variable "prod_vpc_cidr" {}
variable "nonprod_vpc_name" {}
variable "prod_vpc_name" {}
variable "az_1" {}
variable "az_2" {}
variable "pub_subnet1a_cidr" {}
variable "pub_subnet1b_cidr" {}
variable "priv_subnet1b_cidr" {}
variable "priv_subnet1a_cidr" {}
variable "priv_subnet1_cidr" {}
variable "nonprod_env_prefix" {}
variable "prod_env_prefix" {}
variable "inctance_type" {}
variable "public_key_location" {}
variable "priv_subnet2_cidr" {}