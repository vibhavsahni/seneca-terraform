provider "aws" {
  access_key = var.my_access_key
  secret_key = var.my_secret_key
  region     = var.aws_region
}

# Creates IAM policy to allow creating Elatic IP
resource "aws_iam_policy" "ec2_eip_permissions" {
  name        = "EC2EIPPermissions"
  description = "Permissions to manage EC2 instances and Elastic IPs"

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect = "Allow",
        Action = [
          "ec2:DescribeInstances",
          "ec2:AssociateAddress",
          "ec2:DescribeAddresses",
          "ec2:AllocateAddress",
        ],
        Resource = "*",
      },
    ],
  })
}

# Creates the Non-Prod VPC
resource "aws_vpc" "nonprod_vpc" {
  cidr_block = var.nonprod_vpc_cidr
  tags = {
    Name = var.nonprod_vpc_name
  }
}

# Creates a Prod VPC
resource "aws_vpc" "prod_vpc" {
  cidr_block = var.prod_vpc_cidr
  tags = {
    Name = var.prod_vpc_name
  }
}

# Creates Public Subnet 1a in us-east-1b under nonprod VPC
resource "aws_subnet" "nonprod_public_subnet_1a" {
  vpc_id                  = aws_vpc.nonprod_vpc.id
  cidr_block              = var.pub_subnet1a_cidr
  availability_zone       = var.az_1
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.nonprod_vpc_name}_public_subnet_1a"
  }
}

# Creates Private Subnet 1a in us-east-1b under nonprod VPC
resource "aws_subnet" "nonprod_private_subnet_1a" {
  vpc_id            = aws_vpc.nonprod_vpc.id
  cidr_block        = var.priv_subnet1a_cidr
  availability_zone = var.az_1
  tags = {
    Name = "${var.nonprod_vpc_name}_private_subnet_1a"
  }
}

# Creates Public Subnet 1b in us-east-1c under nonprod VPC
resource "aws_subnet" "nonprod_public_subnet_1b" {
  vpc_id                  = aws_vpc.nonprod_vpc.id
  cidr_block              = var.pub_subnet1b_cidr
  availability_zone       = var.az_2
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.nonprod_vpc_name}_public_subnet_1b"
  }
}

# Creates Private Subnet 1b in us-east-1c under nonprod VPC
resource "aws_subnet" "nonprod_private_subnet_1b" {
  vpc_id            = aws_vpc.nonprod_vpc.id
  cidr_block        = var.priv_subnet1b_cidr
  availability_zone = var.az_2
  tags = {
    Name = "${var.nonprod_vpc_name}_private_subnet_1b"
  }
}

# Creates Private Subnet 1 in us-east-1b under prod VPC
resource "aws_subnet" "prod_private_subnet_1" {
  vpc_id            = aws_vpc.prod_vpc.id
  cidr_block        = var.priv_subnet1_cidr
  availability_zone = var.az_1
  tags = {
    Name = "${var.prod_vpc_name}_private_subnet_1"
  }
}

# Creates Private Subnet 2 in us-east-1c under prod VPC
resource "aws_subnet" "prod_private_subnet_2" {
  vpc_id            = aws_vpc.prod_vpc.id
  cidr_block        = var.priv_subnet2_cidr
  availability_zone = var.az_2
  tags = {
    Name = "${var.prod_vpc_name}_private_subnet_2"
  }
}

# Creates an IGW for the Non-Prod VPC
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.nonprod_vpc.id
  tags = {
    Name = "igw"
  }
}

# Creates a route table in nonprod VPC for public subnets, which will direct traffic to intenet
resource "aws_route_table" "nonprod_public_subnets_rt" {
  vpc_id = aws_vpc.nonprod_vpc.id
  tags = {
    Name = "${var.nonprod_vpc_name}_public_subnet_1_rt"
  }
}

# Creates a route to direct our public subnet rt to IGW
resource "aws_route" "route-pub" {
  route_table_id         = aws_route_table.nonprod_public_subnets_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id

}
# Creates a route table for private subnets
resource "aws_route_table" "nonprod_private_subnets_rt" {
  vpc_id = aws_vpc.nonprod_vpc.id
  tags = {
    Name = "${var.nonprod_vpc_name}_private_subnet_1_rt"
  }
}

# Associates public subnets to the public rt
resource "aws_route_table_association" "nonprod_public_subnet_1a_rt_assoc_1" {
  subnet_id      = aws_subnet.nonprod_public_subnet_1a.id
  route_table_id = aws_route_table.nonprod_public_subnets_rt.id
}

resource "aws_route_table_association" "nonprod_public_subnet_1b_rt_assoc_2" {
  subnet_id      = aws_subnet.nonprod_public_subnet_1b.id
  route_table_id = aws_route_table.nonprod_public_subnets_rt.id
}

# Associates private subnets to the private rt
resource "aws_route_table_association" "nonprod_private_subnet_1a_rt_assoc_2" {
  subnet_id      = aws_subnet.nonprod_private_subnet_1a.id
  route_table_id = aws_route_table.nonprod_private_subnets_rt.id
}
resource "aws_route_table_association" "nonprod_private_subnet_1b_rt_assoc_2" {
  subnet_id      = aws_subnet.nonprod_private_subnet_1b.id
  route_table_id = aws_route_table.nonprod_private_subnets_rt.id
}

# Creates a route table in prod VPC
resource "aws_route_table" "prod_private_subnets_rt" {
  vpc_id = aws_vpc.prod_vpc.id
  tags = {
    Name = "${var.prod_vpc_name}_private_subnet_rt"
  }
}

# Associates private subnets to the private rt
resource "aws_route_table_association" "prod_private_subnet_1_rt_assoc" {
  subnet_id      = aws_subnet.prod_private_subnet_1.id
  route_table_id = aws_route_table.prod_private_subnets_rt.id
}
resource "aws_route_table_association" "prod_private_subnet_2_rt_assoc" {
  subnet_id      = aws_subnet.prod_private_subnet_2.id
  route_table_id = aws_route_table.prod_private_subnets_rt.id
}

# Creates a NAT gateway in Public Subnet 1a in us-east-1b under nonprod VPC
# 1st - creating an Elastic IP for NAT Gateway
resource "aws_eip" "nat_eip" {
  domain = "vpc"
}

# 2nd - creating the NAT Gateway
resource "aws_nat_gateway" "nonprod_nat_gateway" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.nonprod_public_subnet_1a.id
}

# Creates a security group for the bastion host instance
resource "aws_security_group" "bastion_sg" {
  name_prefix = "bastion-"
  tags = {
    Name = "Bastion_sg"
  }
  vpc_id = aws_vpc.nonprod_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # This should ideally only include the cidr used by Seneca/any organization, but have kept it anyone as the architecture doesn't define any cidr.

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"          # All protocols
    cidr_blocks = ["0.0.0.0/0"] # Allow traffic to anywhere
  }
}

# Fetching AMI Data and storing it for idempotation in the instance 
data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

# Creates a key pair
resource "aws_key_pair" "ssh-key" {
  key_name   = "bastion_key"
  public_key = file(var.public_key_location)
}

# Creates the bastion host instance with Amazon Linux running on it
resource "aws_instance" "bastion" {
  ami                         = data.aws_ami.latest-amazon-linux-image.id
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.nonprod_public_subnet_1b.id
  key_name                    = aws_key_pair.ssh-key.key_name
  user_data                   = file("key.sh")
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  availability_zone           = var.az_2
  associate_public_ip_address = true
  tags = {
    Name = "Bastion"
  }
}

# Seurity gp with Ingress rule that allows traffic from bastion host security group
resource "aws_security_group" "nonprod_private_VM_sg" {
  name        = "nonprod_private_VM_sg"
  description = "Security group for private instances"
  vpc_id      = aws_vpc.nonprod_vpc.id
  tags = {
    Name = "${var.nonprod_vpc_name}_private_VM_sg"
  }

  # Ingress rule that allows SSH + HTTP from bastion host's security group
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_sg.id]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_sg.id]
  }

}

# Creates VM1 in private Subnet 1a in us-east-1b under nonprod VPC
resource "aws_instance" "VM1" {
  ami                    = data.aws_ami.latest-amazon-linux-image.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.nonprod_private_subnet_1a.id
  key_name               = aws_key_pair.ssh-key.key_name
  vpc_security_group_ids = [aws_security_group.nonprod_private_VM_sg.id]
  availability_zone      = var.az_1
  tags = {
    Name = "${var.nonprod_vpc_name}-VM1"
  }

}

# Creates VM2 in private Subnet 1b in us-east-1c under nonprod VPC
resource "aws_instance" "VM2" {
  ami                    = data.aws_ami.latest-amazon-linux-image.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.nonprod_private_subnet_1b.id
  key_name               = aws_key_pair.ssh-key.key_name
  vpc_security_group_ids = [aws_security_group.nonprod_private_VM_sg.id]
  availability_zone      = var.az_2
  tags = {
    Name = "${var.nonprod_vpc_name}-VM2"
  }
}

# Creates VPC Peering connection
resource "aws_vpc_peering_connection" "vpc_peering" {
  vpc_id      = aws_vpc.nonprod_vpc.id
  peer_vpc_id = aws_vpc.prod_vpc.id
  auto_accept = true
}

# Creates route tables
resource "aws_route" "route_to_nonprod" {
  route_table_id            = aws_route_table.prod_private_subnets_rt.id
  destination_cidr_block    = aws_vpc.nonprod_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc_peering.id
}

resource "aws_route" "route_to_prod" {
  route_table_id            = aws_route_table.nonprod_private_subnets_rt.id
  destination_cidr_block    = aws_vpc.prod_vpc.cidr_block
  vpc_peering_connection_id = aws_vpc_peering_connection.vpc_peering.id
}

# Creates SG for VMs in Prod VPC
resource "aws_security_group" "prod_private_VM_sg" {
  name        = "prod_private_VM_sg"
  description = "Security group for private instances in Prod"
  vpc_id      = aws_vpc.prod_vpc.id
  tags = {
    Name = "${var.prod_vpc_name}_private_VM_sg"
  }

  # Ingress rule that allows SSH from bastion host security group
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.bastion_sg.id]
  }
}

# Creates VM1 in Private Subnet 1 of us-east-1b under prod VPC
resource "aws_instance" "prod-VM1" {
  ami                    = data.aws_ami.latest-amazon-linux-image.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.prod_private_subnet_1.id
  key_name               = aws_key_pair.ssh-key.key_name
  vpc_security_group_ids = [aws_security_group.prod_private_VM_sg.id]
  availability_zone      = "us-east-1b"
  tags = {
    Name = "${var.prod_vpc_name}-VM1"
  }
}

# Creates VM2 in Private Subnet 2 of us-east-1c under prod VPC
resource "aws_instance" "prod-VM2" {
  ami           = data.aws_ami.latest-amazon-linux-image.id
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.prod_private_subnet_2.id
  user_data     = file("SQL.sh")
  key_name               = aws_key_pair.ssh-key.key_name
  vpc_security_group_ids = [aws_security_group.prod_private_VM_sg.id]
  availability_zone      = var.az_2
  tags = {
    Name = "${var.prod_vpc_name}-VM2"
  }
}