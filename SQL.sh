#!/bin/bash

# Update the package repositories
sudo yum update -y

# Install required dependencies
sudo yum install -y mssql-server

# Run the SQL Server setup
sudo /opt/mssql/bin/mssql-conf setup

# Start the SQL Server service
sudo systemctl start mssql-server

# Enable SQL Server to start on boot
sudo systemctl enable mssql-server

# Print out status information
sudo systemctl status mssql-server --no-pager
